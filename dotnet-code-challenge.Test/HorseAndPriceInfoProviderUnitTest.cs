using System.Collections.Generic;
using System.Threading.Tasks;
using Beteasy.Business;
using Beteasy.Core.Intefaces;
using Beteasy.Core.Model.Caufield;
using Beteasy.Core.Model.Wolferhampton;
using NSubstitute;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class HorseAndPriceInfoProviderUnitTest
    {
        private readonly IRaceDataLoader _raceDataLoader;

        public HorseAndPriceInfoProviderUnitTest()
        {
            _raceDataLoader = Substitute.For<IRaceDataLoader>();
        }

        [Fact(DisplayName = "If No data loaded then empty object is returned")]
        public void Test1()
        {
            _raceDataLoader.LoadCaufieldXmlRaceData().Returns(Task.FromResult<CaufieldXmlData>(null));
            _raceDataLoader.LoadWolferhamptonRaceData().Returns(Task.FromResult<Wolferhampton>(null));

            var horseAndPriceInfoProvider = new HorseAndPriceInfoProvider(_raceDataLoader);
            var result = horseAndPriceInfoProvider.GetHorsePricesInAscOrder().Result;

            Assert.True(result.Count == 0);
        }

        [Fact(DisplayName = "If only Wolferhampton data present then only records from woflerhampton returned sorted by price")]
        public void Test2()
        {
            _raceDataLoader.LoadWolferhamptonRaceData().Returns(Task.FromResult<Wolferhampton>(GetMockedWolferHamptonData()));
            _raceDataLoader.LoadCaufieldXmlRaceData().Returns(Task.FromResult<CaufieldXmlData>(null));
            

            var horseAndPriceInfoProvider = new HorseAndPriceInfoProvider(_raceDataLoader);
            var result = horseAndPriceInfoProvider.GetHorsePricesInAscOrder().Result;

            Assert.True(result.Count == 4);
            Assert.True(result[0].Price == 2 && result[1].Price == 2.1 && result[2].Price == 2.1 && result[3].Price == 10);
        }

        [Fact(DisplayName = "If only Caufield data present then only records from Caufield returned sorted by price")]
        public void Test3()
        {
            _raceDataLoader.LoadWolferhamptonRaceData().Returns(Task.FromResult<Wolferhampton>(null));
            _raceDataLoader.LoadCaufieldXmlRaceData().Returns(Task.FromResult<CaufieldXmlData>(GetCaufieldXmlData()));


            var horseAndPriceInfoProvider = new HorseAndPriceInfoProvider(_raceDataLoader);
            var result = horseAndPriceInfoProvider.GetHorsePricesInAscOrder().Result;

            Assert.True(result.Count == 4);
            Assert.True(result[0].Price == 1 && result[1].Price == 3 && result[2].Price == 5 && result[3].Price == 5);
        }

        [Fact(DisplayName = "If data is present it is sorted ")]
        public void Test4()
        {
            _raceDataLoader.LoadWolferhamptonRaceData().Returns(Task.FromResult<Wolferhampton>(GetMockedWolferHamptonData()));
            _raceDataLoader.LoadCaufieldXmlRaceData().Returns(Task.FromResult<CaufieldXmlData>(GetCaufieldXmlData()));


            var horseAndPriceInfoProvider = new HorseAndPriceInfoProvider(_raceDataLoader);
            var result = horseAndPriceInfoProvider.GetHorsePricesInAscOrder().Result;

            Assert.True(result.Count == 8);
            Assert.True(result[0].Price == 1 && result[7].Price == 10);
        }

        private static Wolferhampton GetMockedWolferHamptonData()
        {
            return new Wolferhampton()
            {
                RawData = new RawData()
                {
                    Markets = new List<Market>()
                    {
                        new Market()
                        {
                            Selections = new List<Selection>()
                            {
                                new Selection()
                                {
                                    Price = 10,
                                    Tags = new SelectionTags() {Name = "Horse with Price 10"}
                                },
                                new Selection()
                                {
                                    Price = 2,
                                    Tags = new SelectionTags() {Name = "Horse with Price 2"}
                                },
                                new Selection()
                                {
                                    Price = 2.1,
                                    Tags = new SelectionTags() {Name = "Horse with Price 2.1"}
                                },
                                new Selection()
                                {
                                    Price = 2.1,
                                    Tags = new SelectionTags() {Name = "Horse Price 2.1"}
                                }
                            }
                        }
                    }
                }
            };
        }

        private static CaufieldXmlData GetCaufieldXmlData()
        {
            return new CaufieldXmlData()
            {
                Races = new Races()
                {
                    Race = new Race()
                    {
                        Number = "1",
                        Horses = new List<Horse>()
                        {
                            new Horse(){Name = "Danny", Number = "1"},
                            new Horse(){Name = "Nikko", Number = "2"},
                            new Horse(){Name = "SuperKik", Number = "3"},
                            new Horse(){Name = "Freddo", Number = "4"}
                        },
                        Prices = new Prices()
                        {
                            Price = new Price()
                            {
                                Horses = new List<Horse>()
                                {
                                    new Horse()
                                    {
                                        Price = "3",
                                        NumberAttr = "1"
                                    },
                                    new Horse()
                                    {
                                        Price = "1",
                                        NumberAttr = "2"
                                    },
                                    new Horse()
                                    {
                                        Price = "5",
                                        NumberAttr = "3"
                                    },
                                    new Horse()
                                    {
                                        Price = "5",
                                        NumberAttr = "3"
                                    }

                                }
                            }
                        }
                    }
                }
            };
        }
    }
}
