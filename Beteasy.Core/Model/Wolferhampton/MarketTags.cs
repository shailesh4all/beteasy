﻿namespace Beteasy.Core.Model.Wolferhampton
{
    public class MarketTags
    {
        public string Places { get; set; }
        public string Type { get; set; }
    }
}