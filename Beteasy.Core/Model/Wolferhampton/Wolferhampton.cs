﻿using System;

namespace Beteasy.Core.Model.Wolferhampton
{
    public class Wolferhampton
    {
        public string FixtureId { get; set; }
        public DateTime Timestamp { get; set; }
        public RawData RawData { get; set; }
    }
}
