﻿using System;
using System.Collections.Generic;

namespace Beteasy.Core.Model.Wolferhampton
{
    public class RawData
    {
        public string FixtureName { get; set; }
        public string Id { get; set; }
        public DateTime StartTime { get; set; }
        public int Sequence { get; set; }
        public Tags Tags { get; set; }
        public List<Market> Markets { get; set; }
        public List<Participant> Participants { get; set; }
    }
}