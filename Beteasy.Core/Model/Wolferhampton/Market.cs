﻿using System.Collections.Generic;

namespace Beteasy.Core.Model.Wolferhampton
{
    public class Market
    {
        public string Id { get; set; }
        public List<Selection> Selections { get; set; }
        public MarketTags Tags { get; set; }
    }
}