﻿using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlRoot(ElementName = "races")]
    public class Races
    {
        [XmlElement(ElementName = "race")]
        public Race Race { get; set; }
    }


}
