﻿namespace Beteasy.Core.Model.Caufield
{
    public class HorseNumAndName
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}