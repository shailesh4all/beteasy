﻿using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlRoot(ElementName = "weight")]
    public class Weight
    {
        [XmlAttribute(AttributeName = "allocated")]
        public string Allocated { get; set; }
        [XmlAttribute(AttributeName = "total")]
        public string Total { get; set; }
    }
    
}
