﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beteasy.Core.Model.Caufield
{
    public class HorseNumAndPrice
    {
        public double Price { get; set; }
        public string Number { get; set; }
    }
}
