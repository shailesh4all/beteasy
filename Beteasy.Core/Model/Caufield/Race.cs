﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlRoot(ElementName = "race")]
    public class Race
    {
        [XmlElement(ElementName = "NumberOfRunners")]
        public string NumberOfRunners { get; set; }
        [XmlElement(ElementName = "start_time")]
        public string StartTime { get; set; }
        [XmlElement(ElementName = "distance")]
        public Distance Distance { get; set; }
        [XmlArray(ElementName = "horses")]
        public List<Horse> Horses { get; set; }
        [XmlElement(ElementName = "prices")]
        public Prices Prices { get; set; }
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "Status")]
        public string Status { get; set; }
    }


}
