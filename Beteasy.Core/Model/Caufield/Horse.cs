﻿using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlType("horse", IncludeInSchema = true)]
    public class Horse
    {
        [XmlElement(ElementName = "number")]
        public string Number { get; set; }
        [XmlAttribute("number")]
        public string NumberAttr { get; set; }
        [XmlElement(ElementName = "trainer")]
        public Trainer Trainer { get; set; }
        [XmlElement(ElementName = "training_location")]
        public string TrainingLocation { get; set; }
        [XmlElement(ElementName = "owners")]
        public string Owners { get; set; }
        [XmlElement(ElementName = "colours")]
        public string Colours { get; set; }
        [XmlElement(ElementName = "current_blinker_ind")]
        public string CurrentBlinkerInd { get; set; }
        [XmlElement(ElementName = "prizemoney_won")]
        public string PrizeMoneyWon { get; set; }
        [XmlElement(ElementName = "jockey")]
        public Jockey Jockey { get; set; }
        [XmlElement(ElementName = "barrier")]
        public string Barrier { get; set; }
        [XmlElement(ElementName = "weight")]
        public Weight Weight { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "age")]
        public string Age { get; set; }
        [XmlAttribute(AttributeName = "sex")]
        public string Sex { get; set; }
        [XmlAttribute(AttributeName = "colour")]
        public string Colour { get; set; }
        [XmlAttribute(AttributeName = "foaling_date")]
        public string FoalingDate { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "last_four_starts")]
        public string LastFourStarts { get; set; }
        [XmlElement(ElementName = "last_ten_starts")]
        public string LastTenStarts { get; set; }
        [XmlAttribute(AttributeName = "Price")]
        public string Price { get; set; }
    }


}
