﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlRoot(ElementName = "price")]
    public class Price
    {
        [XmlElement(ElementName = "priceType")]
        public string PriceType { get; set; }
        [XmlArray(ElementName = "horses")]
        public List<Horse> Horses { get; set; }
    }
}
