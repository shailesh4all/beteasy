﻿using System.Xml.Serialization;

namespace Beteasy.Core.Model.Caufield
{
    [XmlRoot(ElementName = "jockey")]
    public class Jockey
    {
        [XmlElement(ElementName = "statistics")]
        public Statistics Statistics { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }


}
