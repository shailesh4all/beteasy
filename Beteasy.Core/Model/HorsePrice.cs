﻿namespace Beteasy.Core.Model
{
    public class HorsePrice
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
