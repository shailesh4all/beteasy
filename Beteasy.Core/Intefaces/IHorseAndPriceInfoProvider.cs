﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Beteasy.Core.Model;

namespace Beteasy.Core.Intefaces
{
    public interface IHorseAndPriceInfoProvider
    {
        Task<List<HorsePrice>> GetHorsePricesInAscOrder();
    }
}
