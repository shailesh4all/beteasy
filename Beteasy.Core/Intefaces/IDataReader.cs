﻿namespace Beteasy.Core.Intefaces
{
    public interface IDataReader
    {
        string ReadDataFromFile(string fileName);
    }
}
