﻿using System.Threading.Tasks;
using Beteasy.Core.Model.Caufield;
using Beteasy.Core.Model.Wolferhampton;

namespace Beteasy.Core.Intefaces
{
    public interface IRaceDataLoader
    {
        Task<Wolferhampton> LoadWolferhamptonRaceData();
        Task<CaufieldXmlData> LoadCaufieldXmlRaceData();
    }
}
