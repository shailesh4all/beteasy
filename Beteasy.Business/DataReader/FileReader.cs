﻿using System.IO;
using Beteasy.Core.Intefaces;

namespace Beteasy.Business.DataReader
{
    public class FileReader : IDataReader
    {
        public string ReadDataFromFile(string fileName)
        {
            string dataFromFile;

            using (StreamReader r = File.OpenText(fileName))
            {
                dataFromFile = r.ReadToEnd();
            }

            return dataFromFile;
        }
    }
}
