﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beteasy.Core.Intefaces;
using Beteasy.Core.Model;
using Beteasy.Core.Model.Caufield;
using Beteasy.Core.Model.Wolferhampton;

namespace Beteasy.Business
{
    public class HorseAndPriceInfoProvider : IHorseAndPriceInfoProvider
    {
        private readonly IRaceDataLoader _raceDataLoader;

        public HorseAndPriceInfoProvider(IRaceDataLoader raceDataLoader)
        {
            _raceDataLoader = raceDataLoader ?? throw new ArgumentNullException(nameof(raceDataLoader));
        }

        public async Task<List<HorsePrice>> GetHorsePricesInAscOrder()
        {
            List<HorsePrice> horsePrices = new List<HorsePrice>();
            var wolferhamptonRaceData = await _raceDataLoader.LoadWolferhamptonRaceData();
            if (wolferhamptonRaceData != null)
            {
                var horses = GetHorseInfo(wolferhamptonRaceData);
                if (horses != null && horses.Count > 0)
                    horsePrices.AddRange(horses);
            }

            var caufieldRaceData = await _raceDataLoader.LoadCaufieldXmlRaceData();
            if (caufieldRaceData != null)
            {
                var horses = GetHorseInfo(caufieldRaceData);
                if (horses != null && horses.Count > 0)
                    horsePrices.AddRange(horses);
            }

            return horsePrices.OrderBy(c => c.Price).ToList();
        }

        private List<HorsePrice> GetHorseInfo(Wolferhampton wolferhamptonRaceData)
        {
           var result = wolferhamptonRaceData.RawData.Markets
                .SelectMany(c =>
                    c.Selections.Select(d => 
                        new HorsePrice()
                        {
                            Price = d.Price,
                            Name = d.Tags.Name
                        }
                    )).ToList();
            return result;
        }

        private List<HorsePrice> GetHorseInfo(CaufieldXmlData caufieldXmlData)
        {
            var horseNumAndPrice = caufieldXmlData.Races.Race.Prices.Price.Horses
                .Select(c =>
                    new HorseNumAndPrice
                    {
                            Price = double.Parse(c.Price),
                            Number = c.NumberAttr
                        }
                    ).ToList();
           var horseNumAndName = caufieldXmlData.Races.Race.Horses
                .Select(c => new HorseNumAndName
                {
                    Number = c.Number,
                    Name = c.Name
                }).ToList();
            var result = horseNumAndPrice.Join(horseNumAndName, price => price.Number, name => name.Number, (p, n) =>
                new HorsePrice
                {
                    Name = n.Name,
                    Price = p.Price
                }).ToList();
            return result;
        }

      
    }
}
