﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Beteasy.Core.Intefaces;
using Beteasy.Core.Model.Caufield;
using Beteasy.Core.Model.Wolferhampton;
using Newtonsoft.Json;

namespace Beteasy.Business.Implementation
{
    public class RaceDataLoader : IRaceDataLoader
    {
        private readonly IDataReader _dataReader;

        public RaceDataLoader(IDataReader dataReader)
        {
            _dataReader = dataReader ?? throw new ArgumentNullException(nameof(dataReader));
        }

        //Convert Wolferhampton Json file and create object out of it
        public async Task<Wolferhampton> LoadWolferhamptonRaceData()
        {
            //I am making assumption that file is always coming to same directory with same name
            var result = await Task.Run(() =>
             {
                var dataFromFile = _dataReader.ReadDataFromFile(@"FeedData\Wolferhampton_Race1.json");
                var textReader = new StringReader(dataFromFile);
                JsonSerializer jsonSerializer = new JsonSerializer();
                var wolferhamptonJsonData =
                    (Wolferhampton) jsonSerializer.Deserialize(textReader, typeof(Wolferhampton));
                return wolferhamptonJsonData;
            });
            return result;
        }

        //Convert Caufield Xml file and create object out of it
        public async Task<CaufieldXmlData> LoadCaufieldXmlRaceData()
        {
            //I am making assumption that file is always coming to same directory with same name
            var result = await Task.Run(() =>
            {
                var dataFromFile = _dataReader.ReadDataFromFile(@"FeedData\Caulfield_Race1.xml");
                if (!string.IsNullOrWhiteSpace(dataFromFile))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(CaufieldXmlData));
                    var caufieldXmlData = (CaufieldXmlData) serializer.Deserialize(new StringReader(dataFromFile));
                    return caufieldXmlData;
                }

                return null;
            });
            return result;
        }
    }
}
