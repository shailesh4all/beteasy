﻿using System;
using Beteasy.Business;
using Beteasy.Business.DataReader;
using Beteasy.Business.Implementation;
using Beteasy.Core.Intefaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


namespace dotnet_code_challenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            //setup our DI
            var serviceCollection = new ServiceCollection();
            ConfigureServicesBuilder(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();
            //Invoke Get Horse names in asc order of prices
            try
            {
                var horseAndPriceInfoProviderService = serviceProvider.GetService<IHorseAndPriceInfoProvider>();
                var horsePricesInAscOrder = horseAndPriceInfoProviderService.GetHorsePricesInAscOrder().Result;

                //iterate over result
                foreach (var horse in horsePricesInAscOrder)
                {
                    Console.WriteLine($"Horse Name: {horse.Name}, Price: {horse.Price}");
                }

                Console.WriteLine("\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to get horse prices in ascending order because of " + ex.Message);
            }
        }

        private static void ConfigureServicesBuilder(ServiceCollection serviceCollection)
        {
            // Configure services in container
            serviceCollection.AddSingleton<IDataReader, FileReader>();
            serviceCollection.AddSingleton<IRaceDataLoader, RaceDataLoader>();
            serviceCollection.AddSingleton<IHorseAndPriceInfoProvider, HorseAndPriceInfoProvider>();
        }
    }
}
